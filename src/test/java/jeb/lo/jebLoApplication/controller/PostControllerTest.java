package jeb.lo.jebLoApplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import javassist.NotFoundException;
import jeb.lo.model.EReportReason;
import jeb.lo.model.EStatusPost;
import jeb.lo.model.Report;
import jeb.lo.payload.request.PostCreateUpdateRequest;
import jeb.lo.payload.request.ReportCreateRequest;
import jeb.lo.payload.request.VoteCreateRequest;
import jeb.lo.payload.response.PostGetResponse;
import jeb.lo.payload.response.PostsGetResponse;
import jeb.lo.payload.response.VoteCreateResponse;
import jeb.lo.service.PostService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import javax.naming.AuthenticationException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PostControllerTest {

    private final static String POSTS_PATH = "/posts/";
    private final static String USERNAME_TEST = "bkrych";
    private final static EStatusPost POST_STATUS_TEST = EStatusPost.NOWE;
    private final static Long POST_ID_TEST = 123L;
    private final static Float GPS_VALUE_TEST = 1F;
    private final static String TEXT1_TEST = "Text1";
    private final static String TEXT2_TEST = "Text2";
    private final static String LINE_TEST = "4";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostService service;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void getPostById() throws Exception {
        // given
        var serviceResponse = new PostGetResponse();
        serviceResponse.setStatusPost(POST_STATUS_TEST);

        when(service.getPostById(POST_ID_TEST, USERNAME_TEST)).thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(get(POSTS_PATH + POST_ID_TEST).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.statusPost").value(POST_STATUS_TEST.toString()));
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void getPostByIdWhenNotFoundPost() throws Exception {
        // given
        when(service.getPostById(POST_ID_TEST, USERNAME_TEST)).thenReturn(null);

        // when
        var response = mockMvc.perform(get(POSTS_PATH + POST_ID_TEST).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    void getPostByIdWhenUnauthorizedUser() throws Exception {
        // given
        var serviceResponse = new PostGetResponse();
        serviceResponse.setStatusPost(POST_STATUS_TEST);

        when(service.getPostById(POST_ID_TEST, null)).thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(get(POSTS_PATH + POST_ID_TEST).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.statusPost").value(POST_STATUS_TEST.toString()));
    }

    @Test
    void getAllPostsResponse() throws Exception {
        var serviceResponse = new PostsGetResponse();
        serviceResponse.setStatusPost(POST_STATUS_TEST);

        var fromLD = LocalDate.of(2020, 1, 1);
        var toLD = LocalDate.of(9999, 1, 1);

        when(service.getAllPostsResponse(List.of(POST_STATUS_TEST), null, fromLD.atTime(LocalTime.MIN), toLD.atTime(LocalTime.MAX))).thenReturn(List.of(serviceResponse));

        // when
        var response = mockMvc.perform(get(POSTS_PATH + "?status=NOWE&from=2020-01-01").contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].statusPost").value(POST_STATUS_TEST.toString()));
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addNewPost() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        doNothing().when(service).addPost(payload, USERNAME_TEST);

        // when
        var response = mockMvc.perform(post(POSTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addNewPostWhenNotFound() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        doThrow(NotFoundException.class).when(service).addPost(payload, USERNAME_TEST);

        // when
        var response = mockMvc.perform(post(POSTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addNewPostWhenNotValidRequest() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        // when
        var response = mockMvc.perform(post(POSTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description").value("must not be blank"))
                .andExpect(jsonPath("$.title").value("must not be blank"));
    }

    @Test
    void addNewPostWhenUserUnauthorized() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        // when
        var response = mockMvc.perform(post(POSTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updatePostById() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        doNothing().when(service).updatePost(payload, POST_ID_TEST, USERNAME_TEST);

        // when
        var response = mockMvc.perform(put(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updatePostByIdWhenNotFound() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        doThrow(NotFoundException.class).when(service).updatePost(payload, POST_ID_TEST, USERNAME_TEST);

        // when
        var response = mockMvc.perform(put(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updatePostByIdWhenForbidden() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        doThrow(AuthenticationException.class).when(service).updatePost(payload, POST_ID_TEST, USERNAME_TEST);

        // when
        var response = mockMvc.perform(put(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updatePostByIdWhenNotValidRequest() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        // when
        var response = mockMvc.perform(put(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.description").value("must not be blank"))
                .andExpect(jsonPath("$.title").value("must not be blank"));
    }

    @Test
    void updatePostByIdWhenUserUnauthorized() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLatitude(GPS_VALUE_TEST);
        payload.setLongitude(GPS_VALUE_TEST);
        payload.setDescription(TEXT1_TEST);
        payload.setTitle(TEXT2_TEST);
        payload.setLineNames(List.of(LINE_TEST));

        // when
        var response = mockMvc.perform(put(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addVote() throws Exception {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        var responseService = new VoteCreateResponse();
        responseService.setNumberOfLikes(1L);

        when(service.setVote(payload, POST_ID_TEST, USERNAME_TEST)).thenReturn(responseService);

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/vote")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        //then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfLikes").value(1));
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addVoteWhenBadRequest() throws Exception {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        var responseService = new VoteCreateResponse();
        responseService.setNumberOfLikes(1L);

        when(service.setVote(payload, POST_ID_TEST, USERNAME_TEST)).thenReturn(null);

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/vote")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        //then
        response.andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addVoteWhenNotValidRequest() throws Exception {
        // given
        var payload = new VoteCreateRequest();

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/vote")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        //then
        response.andExpect(status().isBadRequest());
    }

    @Test
    void addVoteWhenUserUnauthorized() throws Exception {
        // given
        var payload = new VoteCreateRequest();

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/vote")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        //then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addReport() throws Exception {
        // given
        var payload = new ReportCreateRequest();
        payload.setContent(TEXT1_TEST);
        payload.setReason(EReportReason.FAULTY);
        when(service.addReport(payload, POST_ID_TEST, USERNAME_TEST)).thenReturn(new Report());

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/report")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addReportWhenNotFound() throws Exception {
        // given
        var payload = new ReportCreateRequest();
        payload.setContent(TEXT1_TEST);
        payload.setReason(EReportReason.FAULTY);

        doThrow(NotFoundException.class).when(service).addReport(payload, POST_ID_TEST, USERNAME_TEST);

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/report")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addReportWhenNotValidRequest() throws Exception {
        // given
        var payload = new ReportCreateRequest();

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/report")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.reason").value("must not be null"));
    }

    @Test
    void addReportWhenUserUnauthorized() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();

        // when
        var response = mockMvc.perform(post(POSTS_PATH + POST_ID_TEST + "/report")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST, roles = {"ADMIN"})
    void deletePost() throws Exception {
        // given
        doNothing().when(service).deletePost(POST_ID_TEST);

        // when
        var response = mockMvc.perform(delete(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void deletePostWhenNoAdmin() throws Exception {
        // given
        doNothing().when(service).deletePost(POST_ID_TEST);

        // when
        var response = mockMvc.perform(delete(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST, roles = {"ADMIN"})
    void deletePostWhenNotFound() throws Exception {
        // given

        doThrow(EmptyResultDataAccessException.class).when(service).deletePost(POST_ID_TEST);

        // when
        var response = mockMvc.perform(delete(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    void deletePostWhenUserUnauthorized() throws Exception {
        // when
        var response = mockMvc.perform(delete(POSTS_PATH + POST_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isUnauthorized());
    }
}
