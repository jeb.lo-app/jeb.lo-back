package jeb.lo.jebLoApplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jeb.lo.payload.request.LoginRequest;
import jeb.lo.payload.request.SignupRequest;
import jeb.lo.payload.response.JwtResponse;
import jeb.lo.repository.UserRepository;
import jeb.lo.service.AuthenticateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AuthControllerTest {
    private final static String USERNAME_TEST = "bkrych";
    private final static String EMAIL_TEST = "bkrych@jeb.lo";
    private final static String NOT_FOUND_TEXT = "NotFound";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository repository;

    @MockBean
    AuthenticateService service;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void signupAdmin() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(Set.of("admin"));

        var serviceResponse = ResponseEntity.ok(new JwtResponse("token",
                1L,
                USERNAME_TEST,
                EMAIL_TEST,
                List.of("admin")));

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(false);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(false);

        when(service
                .authenticateUserWithUsernameAndPassword(USERNAME_TEST, "123456"))
                .thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST))
                .andExpect(jsonPath("$.token").isNotEmpty());
    }

    @Test
    void signupMod() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(Set.of("mod"));

        var serviceResponse = ResponseEntity.ok(new JwtResponse("token",
                1L,
                USERNAME_TEST,
                EMAIL_TEST,
                List.of("mod")));

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(false);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(false);

        when(service
                .authenticateUserWithUsernameAndPassword(USERNAME_TEST, "123456"))
                .thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST))
                .andExpect(jsonPath("$.token").isNotEmpty());
    }

    @Test
    void signupUser() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(Set.of("user"));

        var serviceResponse = ResponseEntity.ok(new JwtResponse("token",
                1L,
                USERNAME_TEST,
                EMAIL_TEST,
                List.of("user")));

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(false);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(false);

        when(service
                .authenticateUserWithUsernameAndPassword(USERNAME_TEST, "123456"))
                .thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST))
                .andExpect(jsonPath("$.token").isNotEmpty());
    }

    @Test
    void signupWithoutRole() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(null);

        var serviceResponse = ResponseEntity.ok(new JwtResponse("token",
                1L,
                USERNAME_TEST,
                EMAIL_TEST,
                List.of("admin")));

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(false);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(false);

        when(service
                .authenticateUserWithUsernameAndPassword(USERNAME_TEST, "123456"))
                .thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST))
                .andExpect(jsonPath("$.token").isNotEmpty());
    }

    @Test
    void signupWhenNotFoundEmail() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(null);

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(false);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(true);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest());
    }

    @Test
    void signupWhenNotFoundUsername() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(null);

        when(repository.existsByUsername(USERNAME_TEST)).thenReturn(true);
        when(repository.existsByEmail(EMAIL_TEST)).thenReturn(false);

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest());
    }

    @Test
    void signupWhenNotValidRequest() throws Exception {
        // given
        var payload = new SignupRequest();

        // when
        var response = mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.username").value("must not be blank"))
                .andExpect(jsonPath("$.email").value("must not be blank"))
                .andExpect(jsonPath("$.password").value("must not be blank"));
    }

    @Test
    void signin() throws Exception {
        // given
        var payload = new SignupRequest();
        payload.setUsername(USERNAME_TEST);
        payload.setEmail(EMAIL_TEST);
        payload.setPassword("123456");
        payload.setRole(Set.of("user"));

        var serviceResponse = ResponseEntity.ok(new JwtResponse("token",
                1L,
                USERNAME_TEST,
                EMAIL_TEST,
                List.of("user")));

        when(service
                .authenticateUserWithUsernameAndPassword(USERNAME_TEST, "123456"))
                .thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(post("/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST))
                .andExpect(jsonPath("$.token").isNotEmpty());
    }

    @Test
    void signinWhenNotValidRequest() throws Exception {
        // given
        var payload = new LoginRequest();

        // when
        var response = mockMvc.perform(post("/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.username").value("must not be blank"))
                .andExpect(jsonPath("$.password").value("must not be blank"));
    }

}
