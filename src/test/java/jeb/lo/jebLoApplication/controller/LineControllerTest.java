package jeb.lo.jebLoApplication.controller;

import jeb.lo.payload.response.LineResponse;
import jeb.lo.service.LineService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class LineControllerTest {
    private final static String LINES_GET_PATH = "/lines/";
    private final static String LINE_ID_TEST = "4";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LineService service;

    @Test
    void getAllLines() throws Exception {
        // given
        var responseBody = new LineResponse();
        responseBody.setLineId(LINE_ID_TEST);

        when(service.getAllLines()).thenReturn(List.of(responseBody));


        // when
        var response = mockMvc.perform(get(LINES_GET_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].lineId").value(LINE_ID_TEST));
    }

    @Test
    void getAllLinesWhenIsEmpty() throws Exception {
        // given

        when(service.getAllLines()).thenReturn(List.of());


        // when
        var response = mockMvc.perform(get(LINES_GET_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

}
