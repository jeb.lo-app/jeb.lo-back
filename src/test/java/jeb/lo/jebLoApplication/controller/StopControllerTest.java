package jeb.lo.jebLoApplication.controller;

import jeb.lo.payload.response.StopResponse;
import jeb.lo.service.StopService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class StopControllerTest {
    private final static String STOPS_GET_PATH = "/stops/";
    private final static String STOP_NAME_TEST = "PL. GRUNWALDZKI";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StopService service;

    @Test
    void getAllLines() throws Exception {
        // given
        var responseBody = new StopResponse();
        responseBody.setStopName(STOP_NAME_TEST);

        when(service.getAllStops()).thenReturn(List.of(responseBody));


        // when
        var response = mockMvc.perform(get(STOPS_GET_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].stopName").value(STOP_NAME_TEST));
    }

    @Test
    void getAllLinesWhenIsEmpty() throws Exception {
        // given

        when(service.getAllStops()).thenReturn(List.of());


        // when
        var response = mockMvc.perform(get(STOPS_GET_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

}