package jeb.lo.jebLoApplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jeb.lo.payload.request.CommentCreateRequest;
import jeb.lo.service.CommentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CommentControllerTest {
    private final static String USERNAME_TEST = "bkrych";
    private final static String COMMENT_POST_PATH = "/comments/";
    private final static String COMMENT_TEXT_TEST = "Test text of comment";
    private final static Long POST_ID_TEST = 123L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService service;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addComment() throws Exception {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);
        when(service.addComment(payload, USERNAME_TEST)).thenReturn(true);

        // when
        var response = mockMvc.perform(post(COMMENT_POST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addCommentWhenNotFoundSomethingInDatabase() throws Exception {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);
        when(service.addComment(payload, USERNAME_TEST)).thenReturn(false);

        // when
        var response = mockMvc.perform(post(COMMENT_POST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void addCommentWhenNotValidRequest() throws Exception {
        // given
        var payload = new CommentCreateRequest();
        payload.setText(COMMENT_TEXT_TEST);
        when(service.addComment(payload, USERNAME_TEST)).thenReturn(false);

        // when
        var response = mockMvc.perform(post(COMMENT_POST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.postId").value("must not be null"));
    }

    @Test
    void addCommentWhenUserUnauthorized() throws Exception {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);

        // when
        var response = mockMvc.perform(post(COMMENT_POST_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isUnauthorized());
    }

}
