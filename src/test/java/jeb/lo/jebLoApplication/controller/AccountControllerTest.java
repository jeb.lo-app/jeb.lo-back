package jeb.lo.jebLoApplication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import jeb.lo.payload.request.AccountUpdateRequest;
import jeb.lo.payload.request.SubscriptionSettingsUpdateRequest;
import jeb.lo.payload.response.AccountGetResponse;
import jeb.lo.payload.response.AccountSettingsGetResponse;
import jeb.lo.service.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    private final static String USERNAME_TEST = "bkrych";
    private final static String NOT_FOUND_TEXT = "NotFound";
    private final static String ACCOUNTS_PATH = "/accounts/";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService service;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void me() throws Exception {
        // given
        var serviceResponse = new AccountGetResponse();
        serviceResponse.setUsername(USERNAME_TEST);

        when(service.getAccountByUsername(USERNAME_TEST)).thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH + "me").contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST));
    }

    @Test
    @WithMockUser(username = NOT_FOUND_TEXT)
    void meWhenUserNotFound() throws Exception {
        // given
        when(service.getAccountByUsername(NOT_FOUND_TEXT)).thenReturn(null);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH + "me").contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    void meWhenUnauthorized() throws Exception {
        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH + "me"));

        // then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    void getAccountByUsername() throws Exception {
        // given
        var serviceResponse = new AccountGetResponse();
        serviceResponse.setUsername(USERNAME_TEST);

        when(service.getAccountByUsername(USERNAME_TEST)).thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH + USERNAME_TEST).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST));
    }

    @Test
    void getAccountByUsernameWhenUserNotFound() throws Exception {
        // given
        when(service.getAccountByUsername(NOT_FOUND_TEXT)).thenReturn(null);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH + NOT_FOUND_TEXT).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void getAccountToUpdate() throws Exception {
        // given
        var serviceResponse = new AccountSettingsGetResponse();
        serviceResponse.setUsername(USERNAME_TEST);

        when(service.getAccountToUpdate(USERNAME_TEST)).thenReturn(serviceResponse);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(USERNAME_TEST));
    }

    @Test
    @WithMockUser(username = NOT_FOUND_TEXT)
    void getAccountToUpdateWhenUserNotFound() throws Exception {
        // given
        when(service.getAccountToUpdate(NOT_FOUND_TEXT)).thenReturn(null);

        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH).contentType(MediaType.APPLICATION_JSON));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    void getAccountToUpdateWhenUnauthorized() throws Exception {
        // when
        var response = mockMvc.perform(get(ACCOUNTS_PATH));

        // then
        response.andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updateAccount() throws Exception {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");
        var subscription = new SubscriptionSettingsUpdateRequest();
        subscription.setStopNames(List.of("Rondo"));
        subscription.setLineNames(List.of());
        payload.setSubscriptionSettings(subscription);

        when(service.updateAccount(payload, USERNAME_TEST)).thenReturn(payload);

        // when
        var response = mockMvc.perform(put(ACCOUNTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isOk())
                .andExpect(jsonPath("$.photo").value("123"))
                .andExpect(jsonPath("$.subscriptionSettings.lineNames", hasSize(0)))
                .andExpect(jsonPath("$.subscriptionSettings.stopNames", hasSize(1)));
    }

    @Test
    @WithMockUser(username = USERNAME_TEST)
    void updateAccountWhenNotValidRequest() throws Exception {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");

        when(service.updateAccount(payload, USERNAME_TEST)).thenReturn(payload);

        // when
        var response = mockMvc.perform(put(ACCOUNTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.subscriptionSettings").value("must not be null"));
    }

    @Test
    @WithMockUser(username = NOT_FOUND_TEXT)
    void updateAccountWhenUserNotFound() throws Exception {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");
        var subscription = new SubscriptionSettingsUpdateRequest();
        subscription.setStopNames(List.of("Rondo"));
        subscription.setLineNames(List.of());
        payload.setSubscriptionSettings(subscription);

        when(service.updateAccount(payload, NOT_FOUND_TEXT)).thenReturn(null);

        // when
        var response = mockMvc.perform(put(ACCOUNTS_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(payload)));

        // then
        response.andExpect(status().isNotFound());
    }

    @Test
    void updateAccountWhenUnauthorized() throws Exception {
        // when
        var response = mockMvc.perform(put(ACCOUNTS_PATH));

        // then
        response.andExpect(status().isUnauthorized());
    }
}
