package jeb.lo.jebLoApplication.service;

import jeb.lo.payload.request.CommentCreateRequest;
import jeb.lo.service.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.AFTER_METHOD;

@SpringBootTest
class CommentServiceTest {
    private final static String USERNAME_TEST = "bkrych";
    private final static String NO_ACCOUNT_USER = "no-account";
    private final static String NOT_FOUND_TEST = "NotFound";
    private final static String COMMENT_TEXT_TEST = "Test text of comment";
    private final static Long POST_ID_TEST = 101L;

    @Autowired
    private CommentService service;

    @Test
    @DirtiesContext(methodMode = AFTER_METHOD)
    void addComment() {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);

        // when
        var response = service.addComment(payload, USERNAME_TEST);

        // then
        assertThat(response).isTrue();
    }

    @Test
    void addCommentWhenNotFoundPost() {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(-999L);
        payload.setText(COMMENT_TEXT_TEST);

        // when
        var response = service.addComment(payload, USERNAME_TEST);

        // then
        assertThat(response).isFalse();
    }

    @Test
    void addCommentWhenNotFoundUser() {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);

        // when
        var response = service.addComment(payload, NOT_FOUND_TEST);

        // then
        assertThat(response).isFalse();
    }

    @Test
    void addCommentWhenNoAccount() {
        // given
        var payload = new CommentCreateRequest();
        payload.setPostId(POST_ID_TEST);
        payload.setText(COMMENT_TEXT_TEST);

        // when
        var response = service.addComment(payload, NO_ACCOUNT_USER);

        // then
        assertThat(response).isFalse();
    }
}
