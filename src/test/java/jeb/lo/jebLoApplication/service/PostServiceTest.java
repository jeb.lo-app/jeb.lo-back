package jeb.lo.jebLoApplication.service;

import javassist.NotFoundException;
import jeb.lo.model.EReportReason;
import jeb.lo.model.EStatusPost;
import jeb.lo.payload.request.PostCreateUpdateRequest;
import jeb.lo.payload.request.ReportCreateRequest;
import jeb.lo.payload.request.VoteCreateRequest;
import jeb.lo.service.FCMService;
import jeb.lo.service.PostService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;

import javax.naming.AuthenticationException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.AFTER_METHOD;

@SpringBootTest
class PostServiceTest {
    private final static String USERNAME_TEST = "bkrych";
    private final static String NOT_FOUND_TEXT = "NotFound";
    private final static EStatusPost POST_STATUS_TEST = EStatusPost.ZAKOŃCZONE;
    private final static Long POST_ID_TEST = 101L;
    private final static String TEXT1_TEST = "Text1";
    private final static String TEXT2_TEST = "Text2";
    private final static String LINE_TEST = "4";
    public static final long NOT_FOUND_POST_ID = -1L;

    @Autowired
    PostService service;

    @MockBean
    FCMService fcmService;

    @Test
    @Transactional
    void getAllPostsWithByLines() {
        // given
        var status = List.of(POST_STATUS_TEST);
        var lines = List.of(LINE_TEST);
        var from = LocalDate.of(2020, 1, 1).atTime(LocalTime.MIN);
        var to = LocalDate.of(9999, 1, 1).atTime(LocalTime.MAX);

        // when
        var response = service.getAllPostsResponse(status, lines, from, to);

        // then
        assertThat(response).hasSize(1);
        assertThat(response.stream().anyMatch(post -> post.getAuthorUsername().equals(USERNAME_TEST))).isTrue();
    }

    @Test
    @Transactional
    void getAllPostsWithoutByLines() {
        // given
        var status = List.of(POST_STATUS_TEST);
        List<String> lines = List.of();
        var from = LocalDate.of(2020, 1, 1).atTime(LocalTime.MIN);
        var to = LocalDate.of(9999, 1, 1).atTime(LocalTime.MAX);

        // when
        var response = service.getAllPostsResponse(status, lines, from, to);

        // then
        assertThat(response).hasSize(2);
        assertThat(response.stream().anyMatch(post -> post.getAuthorUsername().equals(USERNAME_TEST))).isTrue();
    }

    @Test
    @Transactional
    void getPostById() {
        // when
        var response = service.getPostById(POST_ID_TEST, USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getIsLike()).isTrue();
        assertThat(response.getComments()).hasSize(3);
        assertThat(response.getStatusPost()).isEqualTo(POST_STATUS_TEST);
        assertThat(response.getAuthorUsername()).isEqualTo(USERNAME_TEST);
    }

    @Test
    @Transactional
    void getPostByIdWhenNotFound() {
        // when
        var response = service.getPostById(NOT_FOUND_POST_ID, USERNAME_TEST);

        // then
        assertThat(response).isNull();
    }

    @Test
    @Transactional
    void getPostByIdWithoutUsername() {
        // when
        var response = service.getPostById(POST_ID_TEST, null);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getIsLike()).isNull();
        assertThat(response.getComments()).hasSize(3);
        assertThat(response.getStatusPost()).isEqualTo(POST_STATUS_TEST);
        assertThat(response.getAuthorUsername()).isEqualTo(USERNAME_TEST);
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void addPost() throws NotFoundException {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        var from = LocalDate.of(2020, 1, 1).atTime(LocalTime.MIN);
        var to = LocalDate.of(9999, 1, 1).atTime(LocalTime.MAX);

        // when
        service.addPost(payload, USERNAME_TEST);
        var checkPosts = service.getAllPostsResponse(List.of(EStatusPost.NOWE), null, from, to);

        // then
        assertThat(checkPosts).hasSize(1);
        assertThat(checkPosts.get(0).getAuthorUsername()).isEqualTo(USERNAME_TEST);
        assertThat(checkPosts.get(0).getPostedDate()).isBefore(LocalDateTime.now().plusMinutes(1));
        assertThat(checkPosts.get(0).getPostedDate()).isAfter(LocalDateTime.now().minusMinutes(1));
    }

    @Test
    void addPostWhenUserNotFound() {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        // when & then
        assertThrows(NotFoundException.class,
                () -> service.addPost(payload, NOT_FOUND_TEXT));
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void updatePost() throws Exception {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        var from = LocalDate.of(2020, 1, 1).atTime(LocalTime.MIN);
        var to = LocalDate.of(9999, 1, 1).atTime(LocalTime.MAX);

        // when
        service.updatePost(payload, POST_ID_TEST, USERNAME_TEST);
        var checkPosts = service
                .getAllPostsResponse
                        (
                                List.of(POST_STATUS_TEST),
                                List.of(LINE_TEST),
                                from, to
                        );

        // then
        assertThat(checkPosts).hasSize(1);
        assertThat(checkPosts.get(0).getAuthorUsername()).isEqualTo(USERNAME_TEST);
        assertThat(checkPosts.get(0).getTitle()).isEqualTo(TEXT1_TEST);
        assertThat(checkPosts.get(0).getUrlPhotos().get(0)).isEqualTo(TEXT1_TEST);
    }

    @Test
    void updatePostWhenPostNotFound() {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        // when
        assertThrows(NotFoundException.class,
                () -> service.updatePost(payload, NOT_FOUND_POST_ID, USERNAME_TEST));
    }

    @Test
    void updatePostWhenUserNotFound() {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        // when
        assertThrows(NotFoundException.class,
                () -> service.updatePost(payload, POST_ID_TEST, NOT_FOUND_TEXT));
    }

    @Test
    void updatePostWhenUserWithoutAccount() {
        // given
        var payload = new PostCreateUpdateRequest();
        payload.setLineNames(List.of(LINE_TEST));
        payload.setPhotos(List.of(TEXT1_TEST));
        payload.setTitle(TEXT1_TEST);
        payload.setDescription(TEXT2_TEST);

        // when
        assertThrows(AuthenticationException.class,
                () -> service.updatePost(payload, POST_ID_TEST, "no-account"));
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void setVote() {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        // when
        var response = service.setVote(payload, POST_ID_TEST, "mmarciniak");

        // then
        assertThat(response).isNotNull();
        assertThat(response.getNumberOfDislikes()).isEqualTo(1L);
        assertThat(response.getNumberOfLikes()).isEqualTo(3L);
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void setVoteWhenChangeVote() {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(false);

        // when
        var response = service.setVote(payload, POST_ID_TEST, USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getNumberOfDislikes()).isEqualTo(2L);
        assertThat(response.getNumberOfLikes()).isEqualTo(1L);
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void setVoteWhenRemoveVote() {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        // when
        var response = service.setVote(payload, POST_ID_TEST, USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getNumberOfDislikes()).isEqualTo(1L);
        assertThat(response.getNumberOfLikes()).isEqualTo(1L);
    }

    @Test
    void setVoteWhenPostNotFound() {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        // when
        var response = service.setVote(payload, NOT_FOUND_POST_ID, USERNAME_TEST);

        // then
        assertThat(response).isNull();
    }

    @Test
    void setVoteWhenUserNotFound() {
        // given
        var payload = new VoteCreateRequest();
        payload.setIsLike(true);

        // when
        var response = service.setVote(payload, POST_ID_TEST, NOT_FOUND_TEXT);

        // then
        assertThat(response).isNull();
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void addReport() throws Exception {
        // given
        var payload = new ReportCreateRequest();
        payload.setReason(EReportReason.FAULTY);

        // when
        var response = service.addReport(payload, POST_ID_TEST, USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getReason()).isEqualTo(EReportReason.FAULTY);
        assertThat(response.getAccount().getUser().getUsername()).isEqualTo(USERNAME_TEST);
        assertThat(response.getPost().getId()).isEqualTo(POST_ID_TEST);
    }

    @Test
    void addReportWhenUserNotFound() {
        // given
        var payload = new ReportCreateRequest();
        payload.setReason(EReportReason.FAULTY);

        // when
        assertThrows(NotFoundException.class,
                () -> service.addReport(payload, POST_ID_TEST, NOT_FOUND_TEXT));

    }

    @Test
    void addReportWhenPostNotFound() {
        // given
        var payload = new ReportCreateRequest();
        payload.setReason(EReportReason.FAULTY);

        // when
        assertThrows(NotFoundException.class,
                () -> service.addReport(payload, NOT_FOUND_POST_ID, USERNAME_TEST));

    }

    @Test
    @DirtiesContext(methodMode = AFTER_METHOD)
    void deletePost() {
        // when
        service.deletePost(POST_ID_TEST);

        // then
        var responseService = service.getPostById(POST_ID_TEST, USERNAME_TEST);

        assertThat(responseService).isNull();

    }

    @Test
    void deletePostWhenException() {
        // when
        assertThrows(EmptyResultDataAccessException.class,
                () -> service.deletePost(NOT_FOUND_POST_ID));
    }
}
