package jeb.lo.jebLoApplication.service;

import jeb.lo.payload.request.AccountUpdateRequest;
import jeb.lo.payload.request.SubscriptionSettingsUpdateRequest;
import jeb.lo.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.AFTER_METHOD;
import static org.springframework.test.annotation.DirtiesContext.MethodMode.BEFORE_METHOD;


@SpringBootTest
class AccountServiceTest {

    private final static String USERNAME_TEST = "bkrych";
    private final static String NOT_FOUND_TEXT = "NotFound";

    @Autowired
    private AccountService service;

    @Test
    @Transactional
    @DirtiesContext(methodMode = BEFORE_METHOD)
    void getAccountByUsername() {
        // when
        var response = service.getAccountByUsername(USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getUsername()).isEqualTo(USERNAME_TEST);
        assertThat(response.getNumberOfPosts()).isEqualTo(1);
        assertThat(response.getNumberOfComments()).isEqualTo(2);
    }

    @Test
    void getAccountByUsernameWhenNotFound() {
        // when
        var response = service.getAccountByUsername(NOT_FOUND_TEXT);

        // then
        assertThat(response).isNull();
    }

    @Test
    @Transactional
    void getAccountToUpdate() {
        // when
        var response = service.getAccountToUpdate(USERNAME_TEST);

        // then
        assertThat(response).isNotNull();
        assertThat(response.getUsername()).isEqualTo(USERNAME_TEST);
        assertThat(response.getSubscriptionSettings()).isNotNull();
        assertThat(response.getSubscriptionSettings().getLineNames()).isNotEmpty();
        assertThat(response.getSubscriptionSettings().getStopNames()).isEmpty();
    }

    @Test
    @DirtiesContext(methodMode = BEFORE_METHOD)
    void getAccountToUpdateWhenNotFound() {
        // when
        var response = service.getAccountToUpdate(NOT_FOUND_TEXT);

        // then
        assertThat(response).isNull();
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void updateAccount() {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");
        var subscription = new SubscriptionSettingsUpdateRequest();
        subscription.setStopNames(List.of("Rondo"));
        subscription.setLineNames(List.of());
        payload.setSubscriptionSettings(subscription);

        // when
        var response = service.updateAccount(payload, USERNAME_TEST);

        assertThat(response).isNotNull();
        assertThat(response.getPhoto()).isEqualTo("123");
        assertThat(response.getSubscriptionSettings().getLineNames()).isEmpty();
        assertThat(response.getSubscriptionSettings().getStopNames()).isNotEmpty();
    }

    @Test
    @Transactional
    @DirtiesContext(methodMode = AFTER_METHOD)
    void updateAccountWhenNotExistStop() {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");
        var subscription = new SubscriptionSettingsUpdateRequest();
        subscription.setStopNames(List.of(NOT_FOUND_TEXT));
        subscription.setLineNames(List.of());
        payload.setSubscriptionSettings(subscription);

        // when
        var response = service.updateAccount(payload, USERNAME_TEST);

        assertThat(response).isNotNull();
        assertThat(response.getPhoto()).isEqualTo("123");
        assertThat(response.getSubscriptionSettings().getLineNames()).isEmpty();
        assertThat(response.getSubscriptionSettings().getStopNames()).isEmpty();
    }

    @Transactional
    @Test
    void updateAccountWhenUserNotFound() {
        // given
        var payload = new AccountUpdateRequest();
        payload.setPhoto("123");
        var subscription = new SubscriptionSettingsUpdateRequest();
        subscription.setStopNames(List.of());
        subscription.setLineNames(List.of());
        payload.setSubscriptionSettings(subscription);

        // when
        var response = service.updateAccount(payload, NOT_FOUND_TEXT);

        assertThat(response).isNull();
    }
}
