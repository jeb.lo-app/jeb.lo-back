package jeb.lo.jebLoApplication.service;

import jeb.lo.service.LineService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class LineServiceTest {

    @Autowired
    private LineService service;

    @Test
    void getAllLines() {
        // when
        var response = service.getAllLines();

        // then
        assertThat(response.size()).matches(x -> x == 6 || x > 50);
    }
}
