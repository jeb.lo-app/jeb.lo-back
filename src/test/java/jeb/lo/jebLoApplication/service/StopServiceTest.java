package jeb.lo.jebLoApplication.service;

import jeb.lo.service.StopService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class StopServiceTest {

    @Autowired
    private StopService service;

    @Test
    void getAllLines() {
        // when
        var response = service.getAllStops();

        // then
        assertThat(response.size()).matches(x -> x == 4 || x > 2000);
    }
}
