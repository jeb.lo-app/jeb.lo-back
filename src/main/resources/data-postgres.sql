insert into accounts (id, photo) values
(101,'https://firebasestorage.googleapis.com/v0/b/jeblo-storage.appspot.com/o/profile%2Fkrych.jpg?alt=media&token=7b0cdef1-76e9-4d5b-af66-0286d9a40af8'),
(102,'https://randomuser.me/api/portraits/men/24.jpg'),
(103,'https://randomuser.me/api/portraits/women/4.jpg'),
(104,'https://randomuser.me/api/portraits/women/71.jpg'),
(105,'https://randomuser.me/api/portraits/men/78.jpg'),
(999, 'https://vignette.wikia.nocookie.net/fan-fiction-library/images/1/15/Admin.png/revision/latest?cb=20140917130743'),
(998, 'https://firebasestorage.googleapis.com/v0/b/jeblo-storage.appspot.com/o/profile%2Fmarciniak.jpg?alt=media&token=799b909d-b478-4818-b2de-2eb112cd3478'),
(997, 'https://firebasestorage.googleapis.com/v0/b/jeblo-storage.appspot.com/o/profile%2Fplich.jpg?alt=media&token=a2199bdd-e1ed-42e8-87ff-cb9e6460b371'),
(996, 'https://firebasestorage.googleapis.com/v0/b/jeblo-storage.appspot.com/o/profile%2Fkierzkowski.jpg?alt=media&token=a738a178-5b50-494b-9e0b-3c8de51f9248'),
(995, 'https://images-na.ssl-images-amazon.com/images/I/51cubMMYCAL._SY355_.jpg');

insert into roles (id, name) values
(1, 'ROLE_ADMIN'),
(2, 'ROLE_MODERATOR'),
(3, 'ROLE_USER');

insert into users (id, account_id, email, password, username, joined_date) values
(101,101, 'bartosz.krych@jeb.lo', '$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'bkrych', '2019-12-30 10:41:06.189'),
(102,102, 'morris.mendoza@hotmail.com', '$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'presiden69', '2020-01-23 11:31:06.189'),
(103,103, 'celina.cole@wp.pl', '$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'Galore89', '2020-03-03 21:31:06.189'),
(104,104, 'charlotte.thomas@gmail.com', '$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'ttetho', '2020-01-03 21:31:06.189'),
(105,105, 'budweise@gmail.com', '$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'lopezio83', '2020-01-03 21:32:06.189'),
(999,999,'admin@jeb.lo','$2a$10$8ZOIS0/z0vAcAf6l5W4/derrVTPA.uVdhZpzhV/Yj0OWQTuGghWnu','admin','1996-11-15 17:23:06.189'),
(998,998,'maciej.marciniak@jeb.lo','$2a$10$8ZOIS0/z0vAcAf6l5W4/derrVTPA.uVdhZpzhV/Yj0OWQTuGghWnu','mmarciniak','2020-03-04 00:17:06.189'),
(997,997,'kamil.plich@jeb.lo','$2a$10$8ZOIS0/z0vAcAf6l5W4/derrVTPA.uVdhZpzhV/Yj0OWQTuGghWnu','kplich','2020-03-04 00:17:06.189'),
(996,996,'michal.kierzkowski@jeb.lo','$2a$10$8ZOIS0/z0vAcAf6l5W4/derrVTPA.uVdhZpzhV/Yj0OWQTuGghWnu','mkierzkowski','2020-03-04 00:17:06.189'),
(995,995, 'too.long.too.long@too.long.com','$2a$10$05pESx8s6U1U4y5Sfeq2OeH9O4pdMH6/bIHD2PTZMvAkj1l.GMM6S', 'toolongnamtoremember','2020-03-04 00:17:06.189');

insert into user_roles(user_id, role_id) values
(101, 1),
(102, 1),
(103, 2),
(104, 2),
(105, 3),
(999, 3),
(998, 3),
(997, 3),
(996, 3),
(995, 3);

insert into lines (id, end1, end2) values
('4', 'OPORÓW', 'BISKUPIN'),
('5', 'OPORÓW', 'KSIĄŻĘ MAŁE'),
('10', 'BISKUPIN', 'NIEWIEMGDZIE'),
('11', 'CMENTARZ GRABUSZYŃSKI', 'KROMERA'),
('15', 'PARK POŁUDNIOWY', 'POŚWIĘTNE'),
('17', 'POCZĄTEK PIĘKNEJ PODRÓŻY', 'KONIEC TRASY');

insert into stops (id, stop_code, stop_name, stop_lat, stop_lon)values
(1748, 101, 'Plac Grunwaldzki',14.232, 23.12),
(716, 102, 'Azaliowa',51.16417141, 17.11193640),
(1611, 103, 'Rondo',51.09114944, 17.01787963),
(3452, 104, 'Bzowa (Centrum Zajezdnia)', 51.09760598, 16.99265121);

insert into posts (id, account_id, stop_id, description, latitude, longitude, posted_date, status_post, title) values
(101,101, 1748,'Ale sie odwaliło na placu grunwaldzkim! Motorniczy wyskoczyl z tramwaju i uciekl na wyklad Pana Doktora Fraczkowskiego!',
 51.111753199999995, 17.06064400752848,'2020-02-02 17:06:56.789','ZAKOŃCZONE','PLAC GRUNWALDZKI!'),
(102,102, 716,'Chciałem poszukać azylu na azaliowej bo myslalem ze Azyliowa, a dostałem brzydkie doświdczenie życiowe!!!!!1!11!',
 51.16418141, 17.11193740,'2020-02-03 15:06:56.789','AKTUALNE','Azyl? NIE!!! Azaliowa...'),
(103,103, 1611,'Jechalam na zakupki, na wyprzedaze do Skaja, a tu klops, na rondzie tramwaj padl...',
  51.09021797580834, 17.016400598395343,'2020-02-03 13:06:56.789','ZAKOŃCZONE','I po wyprzedażach!!! Rondo padło.'),
(104,104, 3452,' Jechałam na cmenatrz, chiałam tam zostać.... teraz zostaje mi wrócić do domu i żyć dalej... Na Bzowej jest woodstock? Przynajmniej tak wyglada bo na torach kąpią sie w blocie...',
 51.097865662584304, 16.993461956827208,'2020-02-03 15:06:56.789','AKTUALNE','Błoto się wylało na bzowej :O');

insert into post_photos (id, post_id, url) values
(101,101,'http://zm.org.pl/img/img.097/wroclaw-097-017.jpg'),
(102,101,'https://miejscawewroclawiu.pl/wp-content/uploads/2019/10/wypadek-wro-960x675.jpg'),
(103,101,'https://d-pt.ppstatic.pl/kadry/k/r/5/fa/23/4fbcade86fc16_o,size,640x400,q,71,h,520207.jpg'),
(104,102,'https://d-pt.ppstatic.pl/kadry/k/r/1/66/97/5a26afa7392a0_o,size,640x400,q,71,h,7e437c.jpg'),
(105,102,'https://www.wroclaw.pl/files/cmsdocuments/6609249/wypadek%20mickiewicza1.jpg'),
(106,103,'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQOHBbNGxFKAFFLVNHeaunaz1tiKPOKO7Ts1VS7WFX1SoMJ9z_6&usqp=CAU'),
(107,104,'https://bi.im-g.pl/im/9f/57/18/z25523871V,Smiertelny-wypadek-na-Baltyckiej--Piesza-potracona.jpg');

insert into comments (id, account_id, post_id, commented_date, text) values
(101,101,101,'2020-02-02 17:16:36.134','Tak bylo! Nie zmyslam! XD'),
(102,102,101,'2020-02-02 17:19:36.134','O czym jest dzisiaj wykład? Powiecie Panu Doktorowi, że się spóźnię?'),
(103,103,101,'2020-02-02 17:20:36.134','Zamknal sale na klucz...'),
(104,105,102,'2020-02-02 17:19:36.134','Ale z Ciebie  D Z B A N'),
(105,103,103,'2020-02-02 17:20:36.134','SISTERS! Ja też! Kurde... idziemy gdzieś na kawusie? na ploteczki?'),
(106,105,104,'2020-02-02 17:19:36.134','Idź lepiej do lekarza.'),
(107,101,104,'2020-02-02 17:20:36.134','Jak chciałaś zostać na cmentarzu to mam nadzieję, żeby tylko tam pracować :(');

insert into lines_affected (post_id, line_id, incident_date) values
(101,'4', '2019-12-10 17:06:56.789'),
(101,'10','2019-12-10 17:06:56.789'),
(102,'15','2019-12-10 17:06:56.789'),
(103,'10','2019-12-10 17:06:56.789'),
(104,'11','2019-12-10 17:06:56.789'),
(104,'4','2019-12-10 17:06:56.789'),
(104,'5','2019-12-10 17:06:56.789');

insert into votes (account_id, post_id, is_like) values
                                                     (101,101, true),
                                                     (101,102, true),
                                                     (102,101, true),
                                                     (102, 102, false),
                                                     (102, 103, true),
                                                     (105, 102, true),
                                                     (105, 103, true),
                                                     (105, 101, false),
                                                     (101, 104, false),
                                                     (102, 104, false),
                                                     (103, 104, false),
                                                     (105, 104, false),
                                                     (104, 104, true);

insert into line_favorite (account_id, line_id)
values (101, '4'),
       (101, '11');