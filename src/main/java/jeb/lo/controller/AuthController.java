package jeb.lo.controller;

import jeb.lo.model.Account;
import jeb.lo.model.ERole;
import jeb.lo.model.Role;
import jeb.lo.model.User;
import jeb.lo.payload.request.LoginRequest;
import jeb.lo.payload.request.SignupRequest;
import jeb.lo.payload.response.MessageResponse;
import jeb.lo.repository.AccountRepository;
import jeb.lo.repository.RoleRepository;
import jeb.lo.repository.UserRepository;
import jeb.lo.service.AuthenticateService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
    private static final String ERROR_ROLE_IS_NOT_FOUND = "Error: Role is not found.";

    private final UserRepository userRepository;

    private final AccountRepository accountRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final AuthenticateService authenticateService;

    public AuthController(UserRepository userRepository, AccountRepository accountRepository,
                          RoleRepository roleRepository, PasswordEncoder encoder,
                          AuthenticateService authenticateService) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.authenticateService = authenticateService;
    }


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return authenticateService
                .authenticateUserWithUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword());
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		/* When boxed type java.lang.Boolean is used as an expression it will throw NullPointerException
		if the value is null as defined in Java Language Specification §5.1.8 Unboxing Conversion.
		It is safer to avoid such conversion altogether and handle the null value explicitly. */
        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        var user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<Role> roles = generateRoles(signUpRequest.getRole());

        var account = new Account();
        user.setRoles(roles);
        user.setAccount(account);

        accountRepository.saveAndFlush(account);
        userRepository.saveAndFlush(user);

        return authenticateService
                .authenticateUserWithUsernameAndPassword(signUpRequest.getUsername(), signUpRequest.getPassword());
    }

    private Set<Role> generateRoles(Set<String> strRoles) {
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            var userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException(ERROR_ROLE_IS_NOT_FOUND));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        var adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_IS_NOT_FOUND));
                        roles.add(adminRole);
                        break;
                    case "mod":
                        var modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_IS_NOT_FOUND));
                        roles.add(modRole);
                        break;
                    default:
                        var userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException(ERROR_ROLE_IS_NOT_FOUND));
                        roles.add(userRole);
                }
            });
        }
        return roles;
    }
}
