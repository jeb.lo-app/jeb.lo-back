package jeb.lo.controller;

import jeb.lo.payload.request.AccountUpdateRequest;
import jeb.lo.payload.response.AccountGetResponse;
import jeb.lo.payload.response.AccountSettingsGetResponse;
import jeb.lo.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/accounts")
@CrossOrigin
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PutMapping("/")
    public ResponseEntity<AccountUpdateRequest> updateAccount(@RequestBody @Valid AccountUpdateRequest accountUpdate,
                                                              Authentication authentication) {
        var updated = accountService.updateAccount(accountUpdate, authentication.getName());
        if (updated == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(updated);
    }

    @GetMapping("/")
    public ResponseEntity<AccountSettingsGetResponse> getAccountToUpdate(Authentication authentication) {
        var account = accountService.getAccountToUpdate(authentication.getName());
        if (account == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(account);
    }

    @GetMapping("/{username:.+}")
    public ResponseEntity<AccountGetResponse> getAccountByUsername(@PathVariable(value = "username") String username) {
        var account = accountService.getAccountByUsername(username);
        if (account == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(account);
    }

    @GetMapping("/me")
    public ResponseEntity<AccountGetResponse> getAccountByToken(Authentication authentication) {
        var account = accountService.getAccountByUsername(authentication.getName());
        if (account == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(account);
    }

    @PostMapping("/token/fcm/{token}")
    public ResponseEntity<?> createDeviceToken(@PathVariable(value = "token") String token,
                                               Authentication authentication) {
        try {
            if (accountService.addFCMDeviceToken(token, authentication.getName())) {
                return ResponseEntity.created(URI.create("")).build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/token/fcm/{token}")
    public ResponseEntity<?> deleteDeviceToken(@PathVariable(value = "token") String token,
                                               Authentication authentication) {
        try {
            if (accountService.deleteFCMDeviceToken(token, authentication.getName())) {
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/token/fcm/{token}")
    public ResponseEntity<?> sendTestNotification(@PathVariable(value = "token") String token) {
        try {
            if (accountService.sendTestNotification(token)) {
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
