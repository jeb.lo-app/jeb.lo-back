package jeb.lo.controller;

import jeb.lo.payload.response.LineResponse;
import jeb.lo.service.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/lines")
@CrossOrigin
public class LineController {
    @Autowired
    private LineService lineService;

    @GetMapping("/")
    public ResponseEntity<List<LineResponse>> getAllLines() {
        return ResponseEntity.ok(lineService.getAllLines());
    }
}
