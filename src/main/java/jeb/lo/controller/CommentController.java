package jeb.lo.controller;

import jeb.lo.payload.request.CommentCreateRequest;
import jeb.lo.service.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/comments")
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/")
    public ResponseEntity<?> addComment(@RequestBody @Valid CommentCreateRequest commentToAdd,
                                        Authentication authentication) {
        if (!commentService.addComment(commentToAdd, authentication.getName()))
            return ResponseEntity.badRequest().build();
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
