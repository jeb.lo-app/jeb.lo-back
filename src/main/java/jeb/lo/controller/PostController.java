package jeb.lo.controller;

import javassist.NotFoundException;
import jeb.lo.model.EStatusPost;
import jeb.lo.payload.request.PostCreateUpdateRequest;
import jeb.lo.payload.request.ReportCreateRequest;
import jeb.lo.payload.request.VoteCreateRequest;
import jeb.lo.payload.response.PostGetResponse;
import jeb.lo.payload.response.PostsGetResponse;
import jeb.lo.payload.response.VoteCreateResponse;
import jeb.lo.service.PostService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/posts")
@CrossOrigin
public class PostController {
    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostGetResponse> getPostById(@PathVariable(value = "id") Long id,
                                                       Authentication authentication) {
        var post = postService.getPostById(id,
                authentication == null ?
                        null :
                        authentication.getName());
        if (post == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(post);
    }

    @GetMapping("/")
    public ResponseEntity<List<PostsGetResponse>> getAllPostsResponse
            (@RequestParam(name = "status", required = false, defaultValue = "nowe,aktualne,zakończone") List<EStatusPost> status,
             @RequestParam(name = "lines", required = false) List<String> lines,
             @RequestParam(name = "from", required = false, defaultValue = "2000-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
             @RequestParam(name = "to", required = false, defaultValue = "9999-01-01") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        return ResponseEntity.ok(postService
                .getAllPostsResponse(status, lines, from.atTime(LocalTime.MIN), to.atTime(LocalTime.MAX)));
    }

    @PostMapping("/")
    public ResponseEntity<?> addNewPost(@RequestBody @Valid PostCreateUpdateRequest postCreate,
                                        Authentication authentication) {
        try {
            postService.addPost(postCreate, authentication.getName());
            return ResponseEntity.status(HttpStatus.CREATED).build();

        } catch (NotFoundException notFoundException) {
            return new ResponseEntity<>("Object not found: " + notFoundException.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updatePostById(@PathVariable(value = "id") Long id,
                                            @RequestBody @Valid PostCreateUpdateRequest postUpdate,
                                            Authentication authentication) {
        try {
            postService.updatePost(postUpdate, id, authentication.getName());
            return ResponseEntity.ok().build();
        } catch (AuthenticationException authenticationException) {
            return new ResponseEntity<>("Forbidden!", HttpStatus.FORBIDDEN);
        } catch (NotFoundException notFoundException) {
            return new ResponseEntity<>("Object not found: " + notFoundException.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{id}/vote")
    public ResponseEntity<VoteCreateResponse> addVote(@PathVariable(value = "id") Long postId,
                                                      @RequestBody @Valid VoteCreateRequest voteToAdd,
                                                      Authentication authentication) {
        var vote = postService.setVote(voteToAdd, postId, authentication.getName());
        if (vote == null) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(vote);
    }

    @PostMapping("/{id}/report")
    public ResponseEntity<?> addReport(@PathVariable(value = "id") Long postId,
                                       @RequestBody @Valid ReportCreateRequest reportCreateRequest,
                                       Authentication authentication) {
        try {
            postService.addReport(reportCreateRequest, postId, authentication.getName());
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (NotFoundException notFoundException) {
            return new ResponseEntity<>("Object not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePost(@PathVariable(value = "id") Long postId) {
        try {
            postService.deletePost(postId);
            return ResponseEntity.ok().build();
        } catch (EmptyResultDataAccessException notFoundException) {
            return ResponseEntity.notFound().build();
        }
    }
}
