package jeb.lo.configuration;

import jeb.lo.model.EStatusPost;
import org.springframework.core.convert.converter.Converter;


public class StringToEStatusPost implements Converter<String, EStatusPost> {
    @Override
    public EStatusPost convert(String source) {
        try {
            return EStatusPost.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
