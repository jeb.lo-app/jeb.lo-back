package jeb.lo.configuration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FCMInitializer {

    final Logger logger = LoggerFactory.getLogger(FCMInitializer.class);

    @Value("${app.firebase-configuration.stream}")
    String stream;


    @PostConstruct
    public void initialize() {
        try {

            InputStream configFileInputStream = new ByteArrayInputStream(Base64.getDecoder().decode(stream.getBytes()));
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(configFileInputStream)).build();

            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("Firebase application has been initialized");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}
