package jeb.lo.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.temporal.Temporal;

@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket controllersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(
                        RequestHandlerSelectors.basePackage(
                                "jeb.lo.controller"))
                .build()
                .apiInfo(apiInfo())
                .directModelSubstitute(Temporal.class, String.class);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("jeb.lo")
                .description("")
                .version("0.0.1")
                .contact(new Contact("jeb.lo team", "", ""))
                .build();
    }
}
