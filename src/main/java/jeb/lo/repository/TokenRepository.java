package jeb.lo.repository;

import jeb.lo.model.Account;
import jeb.lo.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;


public interface TokenRepository extends JpaRepository<Token, Long> {
    @Transactional
    int deleteByTokenAndAccount(String token, Account account);
}
