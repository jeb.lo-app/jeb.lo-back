package jeb.lo.repository;

import jeb.lo.model.Stop;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StopRepository extends JpaRepository<Stop, Integer> {
    Optional<Stop> findFirstByStopName(String stopName);
}
