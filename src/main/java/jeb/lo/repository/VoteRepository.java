package jeb.lo.repository;

import jeb.lo.model.Account;
import jeb.lo.model.Post;
import jeb.lo.model.Vote;
import jeb.lo.model.VoteKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, VoteKey> {

    @Query("select count(*) from Vote v where v.isLike = ?1 and v.post = ?2")
    Long countByIsLikeAndPost(Boolean isLike, Post post);

    Optional<Vote> findByAccountAndPost(Account account, Post post);
}
