package jeb.lo.repository;

import jeb.lo.model.Report;
import jeb.lo.model.ReportKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, ReportKey> {
}
