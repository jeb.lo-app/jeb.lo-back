package jeb.lo.repository;

import jeb.lo.model.EStatusPost;
import jeb.lo.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long>, JpaSpecificationExecutor<Post> {
    List<Post> findDistinctAllByStatusPostInAndLineAffectedList_LineIdInAndPostedDateBetweenOrderByPostedDateDesc
            (Collection<EStatusPost> statusPost, Collection<String> lines, LocalDateTime from, LocalDateTime to);

    List<Post> findAllByStatusPostInAndPostedDateBetweenOrderByPostedDateDesc
            (Collection<EStatusPost> statusPost, LocalDateTime from, LocalDateTime to);

    List<Post> findAllByPostedDateBeforeAndStatusPost(LocalDateTime ldt, EStatusPost status);
}
