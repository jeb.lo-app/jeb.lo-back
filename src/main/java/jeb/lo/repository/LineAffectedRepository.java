package jeb.lo.repository;

import jeb.lo.model.LineAffected;
import jeb.lo.model.LineAffectedKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LineAffectedRepository extends JpaRepository<LineAffected, LineAffectedKey> {
    List<LineAffected> findAllByPostId(Long postId);
}
