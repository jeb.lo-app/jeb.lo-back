package jeb.lo.jobs.posts;

import jeb.lo.model.EStatusPost;
import jeb.lo.repository.PostRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class UpdatePostsAfterFifteenMinutes {
    final PostRepository postRepository;

    @Scheduled(fixedRate = 60_000)
    public void run() {
        LoggerFactory.getLogger(this.getClass().getName()).info("Updating posts...");
        postRepository.findAllByPostedDateBeforeAndStatusPost(LocalDateTime.now().minusMinutes(15), EStatusPost.NOWE)
                .forEach(post -> {
                    post.setStatusPost(EStatusPost.AKTUALNE);
                    postRepository.save(post);
                });
        LoggerFactory.getLogger(this.getClass().getName()).info("Updating posts... Complete");
    }
}
