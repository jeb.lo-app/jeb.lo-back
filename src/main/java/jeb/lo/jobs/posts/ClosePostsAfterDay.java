package jeb.lo.jobs.posts;

import jeb.lo.model.EStatusPost;
import jeb.lo.repository.PostRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class ClosePostsAfterDay {
    final PostRepository postRepository;

    @Scheduled(cron = "0 0 1 * * ?")
    public void run() {
        LoggerFactory.getLogger(this.getClass().getName()).info("Closing posts...");
        postRepository.findAllByPostedDateBeforeAndStatusPost(LocalDateTime.now().minusDays(1), EStatusPost.AKTUALNE)
                .forEach(post -> {
                    post.setStatusPost(EStatusPost.ZAKOŃCZONE);
                    postRepository.save(post);
                });
        LoggerFactory.getLogger(this.getClass().getName()).info("Closing posts... Complete");
    }
}
