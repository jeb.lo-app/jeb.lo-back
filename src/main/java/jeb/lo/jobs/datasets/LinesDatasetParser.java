package jeb.lo.jobs.datasets;

import jeb.lo.model.Line;
import jeb.lo.repository.LineRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;

@Component
public class LinesDatasetParser {
    @Value("${app.datasets.mpk.lines}")
    String resourceURL;

    final LineRepository lineRepository;

    public LinesDatasetParser(LineRepository lineRepository) {
        this.lineRepository = lineRepository;
    }

    @Scheduled(fixedDelay = 86400000)
    public void run() {
        try {
            Resource lineResource = new UrlResource(resourceURL);
            var f = ParserUtils.getFileFromRemote(lineResource, "routes.txt", "");

            if (f != null) {
                LoggerFactory.getLogger(LinesDatasetParser.class).info("Updating lines");
                Files.lines(f.toPath()).skip(1).forEach(this::processLine);
            } else {
                LoggerFactory.getLogger(LinesDatasetParser.class).error("No lines file created!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processLine(String stringLine) {
        String[] parts = stringLine.split(",");
        String[] stops = parts[4].split("\\|")[0].split(" - ");

        var newLine = new Line();
        newLine.setId(parts[0]);
        newLine.setEnd1(stops[0].substring(1));
        newLine.setEnd2(stops[stops.length - 1]);
        lineRepository.save(newLine);
    }
}
