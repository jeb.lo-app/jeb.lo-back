package jeb.lo.jobs.datasets;

import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ParserUtils {
    public static File getFileFromRemote(Resource resource, String neededFile, String prefix) throws IOException {
        var zis = new ZipInputStream(resource.getInputStream());
        var zipEntry = zis.getNextEntry();
        var destDir = new File("src/main/resources");
        var buffer = new byte[1024];
        while (zipEntry != null) {
            if (!zipEntry.getName().equals(neededFile)) {
                zipEntry = zis.getNextEntry();
                continue;
            }

            var newFile = newFile(destDir, zipEntry, prefix);
            var fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zis.closeEntry();
            zis.close();
            return newFile;
        }
        return null;
    }

    private static File newFile(File destinationDir, ZipEntry zipEntry, String prefix) throws IOException {
        var destFile = new File(destinationDir, prefix + zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + prefix + zipEntry.getName());
        }


        return destFile;
    }
}
