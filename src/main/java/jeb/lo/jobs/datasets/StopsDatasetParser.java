package jeb.lo.jobs.datasets;

import jeb.lo.model.Stop;
import jeb.lo.repository.StopRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class StopsDatasetParser {
    @Value("${app.datasets.mpk.lines}")
    String resourceURL;

    final StopRepository stopRepository;

    public StopsDatasetParser(StopRepository stopRepository) {
        this.stopRepository = stopRepository;
    }

    @Scheduled(fixedDelay = 86400000)
    public void run() {
        try {
            Resource stopResource = new UrlResource(resourceURL);
            File f = ParserUtils.getFileFromRemote(stopResource, "stops.txt", "");

            if (f != null) {
                LoggerFactory.getLogger(StopsDatasetParser.class).info("Updating stops");
                Files.lines(f.toPath()).skip(1).forEach(this::processLine);
            } else {
                LoggerFactory.getLogger(StopsDatasetParser.class).error("No stops file created!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processLine(String stringLine) {
        String[] parts = stringLine.split(",");
        Stop newStop = new Stop();
        newStop.setId(parts[0]);
        newStop.setStopCode(parts[1]);
        newStop.setStopName(parts[2].replaceAll("\"", ""));
        newStop.setStopLat(Float.parseFloat(parts[3]));
        newStop.setStopLon(Float.parseFloat(parts[4]));

        stopRepository.save(newStop);
    }
}
