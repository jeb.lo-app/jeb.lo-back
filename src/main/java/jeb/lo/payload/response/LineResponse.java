package jeb.lo.payload.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LineResponse {
    String lineId;
    String end1;
    String end2;
}
