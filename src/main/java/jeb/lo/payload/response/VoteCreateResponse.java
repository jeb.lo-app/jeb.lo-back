package jeb.lo.payload.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VoteCreateResponse {
    Long numberOfLikes;
    Long numberOfDislikes;
}
