package jeb.lo.payload.response;

import lombok.Data;

@Data
public class StopResponse {
    int stopId;
    int stopCode;
    String stopName;
    float stopLat;
    float stopLon;
}
