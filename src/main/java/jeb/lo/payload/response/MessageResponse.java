package jeb.lo.payload.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageResponse {
    String message;

    public MessageResponse(String message) {
        this.message = message;
    }
}