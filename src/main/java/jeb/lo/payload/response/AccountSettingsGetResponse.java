package jeb.lo.payload.response;

import jeb.lo.payload.request.SubscriptionSettingsUpdateRequest;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountSettingsGetResponse {
    String username;
    String email;
    String photo;
    SubscriptionSettingsUpdateRequest subscriptionSettings;
}
