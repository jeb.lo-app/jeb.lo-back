package jeb.lo.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import jeb.lo.model.EStatusPost;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostInAccountGetResponse {
    Long id;
    String title;
    String description;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    LocalDateTime postedDate;
    EStatusPost statusPost;

    List<String> linesBlocked;
    Long likes;
    Long dislikes;
    Long numberOfComments;
}
