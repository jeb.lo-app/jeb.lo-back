package jeb.lo.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountGetResponse {
    String username;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    LocalDateTime joinedDate;

    String photoSource;
    Long numberOfPosts;
    Long numberOfComments;
    Long likes;
    Long dislikes;
    List<String> favoriteLines;
    List<String> favoriteStops;
    List<PostInAccountGetResponse> accountPosts;
}
