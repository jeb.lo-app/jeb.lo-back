package jeb.lo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NotificationPayload {
    String target;
    String title;
    String content;
    String body;
}
