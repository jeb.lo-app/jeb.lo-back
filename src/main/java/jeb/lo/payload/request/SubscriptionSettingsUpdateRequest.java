package jeb.lo.payload.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubscriptionSettingsUpdateRequest {
    List<String> lineNames;
    List<String> stopNames;
}
