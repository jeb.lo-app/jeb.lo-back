package jeb.lo.payload.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostCreateUpdateRequest {
	@NotBlank
	String title;

	@NotBlank
	String description;

	@NotNull
	Float latitude;

	@NotNull
	Float longitude;

	@NotEmpty
	List<String> lineNames;

	List<String> photos;
}
