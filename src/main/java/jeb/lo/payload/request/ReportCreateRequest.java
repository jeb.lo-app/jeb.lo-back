package jeb.lo.payload.request;

import jeb.lo.model.EReportReason;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReportCreateRequest {

    @NotNull
    EReportReason reason;

    String content;
}
