package jeb.lo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SubscriptionPayload {
    List<String> tokens;
    List<String> topics;
}
