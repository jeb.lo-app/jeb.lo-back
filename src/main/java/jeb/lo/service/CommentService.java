package jeb.lo.service;

import jeb.lo.model.Comment;
import jeb.lo.payload.request.CommentCreateRequest;
import jeb.lo.repository.CommentRepository;
import jeb.lo.repository.PostRepository;
import jeb.lo.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final ModelMapper modelMapper;

    public CommentService(CommentRepository commentRepository, @Lazy UserRepository userRepository,
                          @Lazy PostRepository postRepository, ModelMapper modelMapper) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    public boolean addComment(CommentCreateRequest commentToAdd, String name) {
        var oPost = postRepository.findById(commentToAdd.getPostId());
        if (oPost.isEmpty()) return false;
        var post = oPost.get();

        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) return false;

        var account = oUser.get().getAccount();
        if (account == null) return false;

        var comment = modelMapper.map(commentToAdd, Comment.class);
        comment.setAccount(account);
        comment.setPost(post);
        comment.setId(null);
        commentRepository.save(comment);
        return true;
    }
}
