package jeb.lo.service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import jeb.lo.payload.NotificationPayload;
import jeb.lo.payload.SubscriptionPayload;
import org.springframework.stereotype.Service;

@Service
public class FCMService {
    public void subscribeToTopic(SubscriptionPayload payload) {
        payload.getTopics().forEach(topic -> {
            try {
                FirebaseMessaging.getInstance().subscribeToTopic(payload.getTokens(), topic);
            } catch (FirebaseMessagingException e) {
                e.printStackTrace();
            }
        });
    }

    public void unsubscribeFromTopic(SubscriptionPayload payload) {
        payload.getTopics().forEach(topic -> {
            try {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(payload.getTokens(), topic);
            } catch (FirebaseMessagingException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendPnsToTopic(NotificationPayload notificationPayload) {
        var message = Message.builder()
                .setTopic(notificationPayload.getTarget())
                .putData("content", notificationPayload.getTitle())
                .putData("body", notificationPayload.getBody())
                .build();

        try {
            FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    public void sendPnsToDevice(NotificationPayload notificationPayload) {
        var message = Message.builder()
                .setToken(notificationPayload.getTarget())
                .putData("content", notificationPayload.getTitle())
                .putData("body", notificationPayload.getBody())
                .build();

        try {
            FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }

    }
}
