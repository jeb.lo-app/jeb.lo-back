package jeb.lo.service;

import jeb.lo.payload.response.LineResponse;
import jeb.lo.repository.LineRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LineService {

    private final LineRepository lineRepository;
    private final ModelMapper modelMapper;

    public LineService(LineRepository lineRepository, ModelMapper modelMapper) {
        this.lineRepository = lineRepository;
        this.modelMapper = modelMapper;
    }

    public List<LineResponse> getAllLines() {
        var allLines = lineRepository.findAll();
        return allLines.stream().map(line -> modelMapper.map(line, LineResponse.class)).collect(Collectors.toList());
    }
}
