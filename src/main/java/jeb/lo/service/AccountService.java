package jeb.lo.service;

import javassist.NotFoundException;
import jeb.lo.model.*;
import jeb.lo.payload.NotificationPayload;
import jeb.lo.payload.SubscriptionPayload;
import jeb.lo.payload.request.AccountUpdateRequest;
import jeb.lo.payload.request.SubscriptionSettingsUpdateRequest;
import jeb.lo.payload.response.AccountGetResponse;
import jeb.lo.payload.response.AccountSettingsGetResponse;
import jeb.lo.payload.response.PostInAccountGetResponse;
import jeb.lo.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private static final String USER_NOT_FOUND_EXCEPTION = "User not found";
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final LineRepository lineRepository;
    private final StopRepository stopRepository;
    private final TokenRepository tokenRepository;
    private final FCMService fcmService;

    private final ModelMapper modelMapper;

    public AccountService(@Lazy AccountRepository accountRepository, @Lazy UserRepository userRepository,
                          @Lazy LineRepository lineRepository, @Lazy StopRepository stopRepository,
                          @Lazy TokenRepository tokenRepository, @Lazy FCMService fcmService,
                          ModelMapper modelMapper) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.lineRepository = lineRepository;
        this.stopRepository = stopRepository;
        this.tokenRepository = tokenRepository;
        this.fcmService = fcmService;
        this.modelMapper = modelMapper;
    }

    public AccountGetResponse getAccountByUsername(String username) {
        var oUser = userRepository.findByUsername(username);
        if (oUser.isEmpty()) return null;
        var user = oUser.get();
        return getAccountResponse(user);
    }

    public AccountSettingsGetResponse getAccountToUpdate(String name) {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) return null;
        var user = oUser.get();
        var account = user.getAccount();
        var accountDTO = modelMapper.map(account, AccountSettingsGetResponse.class);

        modelMapper.map(user, accountDTO);

        var subscriptions = new SubscriptionSettingsUpdateRequest();
        subscriptions.setLineNames(account.getLineFavoriteList()
                .stream()
                .map(Line::getId)
                .collect(Collectors.toList()));
        subscriptions.setStopNames(account.getStopFavoriteList()
                .stream()
                .map(Stop::getStopName)
                .collect(Collectors.toList()));

        accountDTO.setSubscriptionSettings(subscriptions);

        return accountDTO;
    }

    public AccountUpdateRequest updateAccount(AccountUpdateRequest accountUpdate, String name) {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) return null;
        var user = oUser.get();

        var accountToSave = user.getAccount();
        modelMapper.map(accountUpdate, accountToSave);

        var subscriptions = accountUpdate.getSubscriptionSettings();
        // filter existed stops and lines
        var favoriteStops = subscriptions.getStopNames().stream()
                .map(stop -> stopRepository.findFirstByStopName(stop).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        var favoriteLines = subscriptions.getLineNames().stream()
                .map(fp -> lineRepository.findById(fp).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        checkTokens(accountToSave, subscriptions);

        accountToSave.setLineFavoriteList(favoriteLines);
        accountToSave.setStopFavoriteList(favoriteStops);
        accountRepository.save(accountToSave);

        accountUpdate.getSubscriptionSettings()
                .setLineNames(favoriteLines.stream()
                        .map(Line::getId)
                        .collect(Collectors.toList()));
        accountUpdate.getSubscriptionSettings()
                .setStopNames(favoriteStops.stream()
                        .map(Stop::getStopName)
                        .collect(Collectors.toList()));

        return accountUpdate;
    }

    public boolean addFCMDeviceToken(String tokenString, String name) throws NotFoundException {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) throw new NotFoundException(USER_NOT_FOUND_EXCEPTION);

        var token = new Token();
        token.setToken(tokenString);
        token.setType(TokenType.FCM);
        token.setAccount(oUser.get().getAccount());
        tokenRepository.saveAndFlush(token);

        var notification = new NotificationPayload(token.getToken(), "Welcome!", "TestWelcome", "TestWelcome!");
        fcmService.sendPnsToDevice(notification);

        return true;
    }

    public boolean deleteFCMDeviceToken(String tokenString, String name) throws NotFoundException {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) throw new NotFoundException(USER_NOT_FOUND_EXCEPTION);

        return tokenRepository.deleteByTokenAndAccount(tokenString, oUser.get().getAccount()) > 0;
    }

    public boolean sendTestNotification(String tokenString) {
        var notification = new NotificationPayload(tokenString, "Welcome!", "TestWelcome", "TestWelcome!");

        try {
            fcmService.sendPnsToDevice(notification);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private AccountGetResponse getAccountResponse(User user) {
        var accountDto = modelMapper.map(user, AccountGetResponse.class);

        accountDto.setPhotoSource(user.getAccount().getPhoto());
        accountDto.setNumberOfPosts((long) user.getAccount().getPosts().size());
        accountDto.setNumberOfComments((long) user.getAccount().getComments().size());
        accountDto.setLikes(user.getAccount().getVotes().stream()
                .map(Vote::getIsLike)
                .count());
        accountDto.setDislikes(user.getAccount().getVotes().stream()
                .filter(v -> !v.getIsLike())
                .count());
        accountDto.setFavoriteLines(user.getAccount().getLineFavoriteList().stream()
                .map(Line::getId)
                .collect(Collectors.toList()));
        accountDto.setFavoriteStops(user.getAccount().getStopFavoriteList().stream()
                .map(Stop::getStopName)
                .collect(Collectors.toList()));

        var posts = user.getAccount().getPosts();

        List<PostInAccountGetResponse> postsDto = posts.stream().map(post -> {
            var postDTO = modelMapper.map(post, PostInAccountGetResponse.class);
            postDTO.setLinesBlocked(post.getLineAffectedList().stream()
                    .map(v -> v.getLine().getId())
                    .collect(Collectors.toList()));
            postDTO.setDislikes(post.getVotes().stream()
                    .filter(vote -> !vote.getIsLike())
                    .count());
            postDTO.setLikes(post.getVotes().stream()
                    .filter(Vote::getIsLike)
                    .count());
            postDTO.setNumberOfComments((long) post.getComments().size());
            return postDTO;
        }).collect(Collectors.toList());

        accountDto.setAccountPosts(postsDto);
        return accountDto;
    }

    private void checkTokens(Account accountToSave, SubscriptionSettingsUpdateRequest subscriptions) {
        List<String> tokens = accountToSave.getTokens().stream().map(Token::getToken).collect(Collectors.toList());
        if (!tokens.isEmpty()) {
            List<String> oldTopics = accountToSave.getLineFavoriteList().stream().map(Line::getId).collect(Collectors.toList());
            if (!oldTopics.isEmpty()) {
                fcmService.unsubscribeFromTopic((new SubscriptionPayload(tokens, oldTopics)));
            }
            if (!subscriptions.getLineNames().isEmpty()) {
                fcmService.subscribeToTopic((new SubscriptionPayload(tokens, subscriptions.getLineNames())));
            }
        }
    }
}
