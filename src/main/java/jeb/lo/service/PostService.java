package jeb.lo.service;

import javassist.NotFoundException;
import jeb.lo.model.*;
import jeb.lo.payload.NotificationPayload;
import jeb.lo.payload.request.PostCreateUpdateRequest;
import jeb.lo.payload.request.ReportCreateRequest;
import jeb.lo.payload.request.VoteCreateRequest;
import jeb.lo.payload.response.CommInPostGetResponse;
import jeb.lo.payload.response.PostGetResponse;
import jeb.lo.payload.response.PostsGetResponse;
import jeb.lo.payload.response.VoteCreateResponse;
import jeb.lo.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {
    private static final String POST_NOT_FOUND_EXCEPTION = "Post not found";
    private static final String USER_NOT_FOUND_EXCEPTION = "User not found";

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final LineAffectedRepository lineAffectedRepository;
    private final LineRepository lineRepository;
    private final VoteRepository voteRepository;
    private final ReportRepository reportRepository;
    private final PostPhotoRepository postPhotoRepository;
    private final FCMService fcmService;
    private final ModelMapper modelMapper;

    public PostService(PostRepository postRepository, @Lazy UserRepository userRepository,
                       @Lazy LineAffectedRepository lineAffectedRepository, @Lazy LineRepository lineRepository,
                       @Lazy VoteRepository voteRepository, @Lazy ReportRepository reportRepository,
                       @Lazy PostPhotoRepository postPhotoRepository, @Lazy FCMService fcmService,
                       ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.lineAffectedRepository = lineAffectedRepository;
        this.lineRepository = lineRepository;
        this.voteRepository = voteRepository;
        this.reportRepository = reportRepository;
        this.postPhotoRepository = postPhotoRepository;
        this.fcmService = fcmService;
        this.modelMapper = modelMapper;
    }

    public List<PostsGetResponse> getAllPostsResponse(List<EStatusPost> statusReq, List<String> linesReq, LocalDateTime fromReq, LocalDateTime toReq) {
        List<Post> posts;

        if (linesReq == null || linesReq.isEmpty()) {
            posts = postRepository.findAllByStatusPostInAndPostedDateBetweenOrderByPostedDateDesc(statusReq, fromReq, toReq);
        } else {
            posts = postRepository.findDistinctAllByStatusPostInAndLineAffectedList_LineIdInAndPostedDateBetweenOrderByPostedDateDesc(statusReq, linesReq, fromReq, toReq);
        }

        return posts.stream().map(this::generatePostsGetResponse).collect(Collectors.toList());
    }

    public PostGetResponse getPostById(Long id, String name) {
        var oPost = postRepository.findById(id);

        if (oPost.isEmpty()) return null;

        var post = oPost.get();
        var postDTO = generatePostGetResponse(post);

        var comments = post.getComments();
        if (!comments.isEmpty()) {
            var commentsInPost = comments.stream()
                    .map(this::generateCommInPost)
                    .collect(Collectors.toList());
            postDTO.setComments(commentsInPost);
        }

        var oUser = userRepository.findByUsername(name);
        if (oUser.isPresent()) {
            var oVote = voteRepository.findByAccountAndPost(oUser.get().getAccount(), post);
            oVote.ifPresent(vote -> postDTO.setIsLike(vote.getIsLike()));
        }

        return postDTO;
    }

    public void addPost(PostCreateUpdateRequest postRequest, String name) throws NotFoundException {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) throw new NotFoundException(USER_NOT_FOUND_EXCEPTION);

        var post = modelMapper.map(postRequest, Post.class);
        post.setAccount(oUser.get().getAccount());
        post.setId(null);

        // for getting id of post
        post = postRepository.saveAndFlush(post);

        post.setLineAffectedList(generateLineAffectedList(postRequest, post));
        post.setPhotos(generatePhotosList(postRequest, post));

        postRepository.save(post);

        postRequest.getLineNames()
                .forEach(lineName ->
                        fcmService.sendPnsToTopic(
                                new NotificationPayload(lineName, postRequest.getTitle(),
                                        postRequest.getDescription(), postRequest.getDescription())
                        ));
    }

    public void updatePost(PostCreateUpdateRequest postRequest, Long id, String name) throws AuthenticationException,
            NotFoundException {
        var oPost = postRepository.findById(id);
        if (oPost.isEmpty()) throw new NotFoundException(POST_NOT_FOUND_EXCEPTION);

        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) throw new NotFoundException(USER_NOT_FOUND_EXCEPTION);

        var post = oPost.get();
        if (!post.getAccount().equals(oUser.get().getAccount())) throw new AuthenticationException("Unauthorized");

        modelMapper.map(postRequest, post);
        lineAffectedRepository.findAllByPostId(id).forEach(lineAffectedRepository::delete);
        post.setLineAffectedList(generateLineAffectedList(postRequest, post));

        post.getPhotos().forEach(postPhoto -> {
            if (postPhoto.getId() != null) {
                postPhotoRepository.deleteById(postPhoto.getId());
            }
        });

        post.setPhotos(generatePhotosList(postRequest, post));

        postRepository.save(post);
    }

    public VoteCreateResponse setVote(VoteCreateRequest voteToAdd, Long postId, String name) {
        var oPost = postRepository.findById(postId);
        if (oPost.isEmpty()) return null;
        var post = oPost.get();

        // get account
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) return null;
        var account = oUser.get().getAccount();

        var response = new VoteCreateResponse();

        // check existing vote
        var existVote = voteRepository.findByAccountAndPost(account, post);
        if (existVote.isPresent()) {
            var existed = existVote.get();
            voteRepository.delete(existed);
            //if the same just remove vote
            if (existed.getIsLike().equals(voteToAdd.getIsLike())) {
                setPostVotes(post, response);
                return response;
            }
        }

        var vote = modelMapper.map(voteToAdd, Vote.class);
        vote.setAccount(account);
        vote.setPost(post);

        voteRepository.save(vote);

        setPostVotes(post, response);
        return response;
    }

    public Report addReport(ReportCreateRequest reportCreateRequest, Long postId, String name) throws NotFoundException {
        var oUser = userRepository.findByUsername(name);
        if (oUser.isEmpty()) throw new NotFoundException(USER_NOT_FOUND_EXCEPTION);

        var oPost = postRepository.findById(postId);
        if (oPost.isEmpty()) throw new NotFoundException(POST_NOT_FOUND_EXCEPTION);
        var post = oPost.get();

        var report = modelMapper.map(reportCreateRequest, Report.class);

        report.setAccount(oUser.get().getAccount());
        report.setPost(post);

        return reportRepository.save(report);
    }

    public void deletePost(Long postId) throws EmptyResultDataAccessException {
        postRepository.deleteById(postId);
    }

    private List<LineAffected> generateLineAffectedList(PostCreateUpdateRequest postRequest, Post post) {
        return postRequest.getLineNames()
                .stream()
                .map(line -> {
                    var la = new LineAffected();
                    lineRepository.findById(line).ifPresent(la::setLine);
                    la.setPost(post);
                    lineAffectedRepository.save(la);
                    return la;
                }).collect(Collectors.toList());
    }

    private List<PostPhoto> generatePhotosList(PostCreateUpdateRequest postRequest, Post post) {
        return postRequest.getPhotos().stream().map(photoUrl -> {
            var pp = new PostPhoto();
            pp.setPost(post);
            pp.setUrl(photoUrl);
            postPhotoRepository.save(pp);
            return pp;
        }).collect(Collectors.toList());
    }

    private PostsGetResponse generatePostsGetResponse(Post post) {
        var postDTO = modelMapper.map(post, PostsGetResponse.class);
        postDTO.setLinesBlocked(post.getLineAffectedList().stream()
                .map(v -> v.getLine().getId())
                .collect(Collectors.toList()));
        postDTO.setAuthorUsername(post.getAccount().getUser().getUsername());
        postDTO.setUrlPhotos(post.getPhotos().stream()
                .map(PostPhoto::getUrl)
                .collect(Collectors.toList()));
        if (post.getComments() != null) {
            postDTO.setNumberOfComments((long) post.getComments().size());
        }
        if (post.getVotes() != null) {
            postDTO.setDislikes(post.getVotes().stream()
                    .filter(vote -> !vote.getIsLike())
                    .count());
            postDTO.setLikes(post.getVotes().stream()
                    .filter(Vote::getIsLike)
                    .count());
        }
        return postDTO;
    }

    private CommInPostGetResponse generateCommInPost(Comment comment) {
        var comm = modelMapper.map(comment, CommInPostGetResponse.class);
        comm.setAuthorUsername(comment.getAccount().getUser().getUsername());
        comm.setAuthorPhoto(comment.getAccount().getPhoto());
        return comm;
    }

    private PostGetResponse generatePostGetResponse(Post post) {
        var postDTO = modelMapper.map(post, PostGetResponse.class);

        postDTO.setLinesBlocked(post.getLineAffectedList().stream()
                .map(v -> v.getLine().getId())
                .collect(Collectors.toList()));
        postDTO.setAuthorUsername(post.getAccount().getUser().getUsername());
        postDTO.setUrlPhotos(post.getPhotos().stream()
                .map(PostPhoto::getUrl)
                .collect(Collectors.toList()));

        postDTO.setDislikes(post.getVotes().stream()
                .filter(vote -> !vote.getIsLike())
                .count());
        postDTO.setLikes(post.getVotes().stream()
                .filter(Vote::getIsLike)
                .count());
        return postDTO;
    }

    private void setPostVotes(Post post, VoteCreateResponse response) {
        response.setNumberOfLikes(voteRepository.countByIsLikeAndPost(true, post));
        response.setNumberOfDislikes(voteRepository.countByIsLikeAndPost(false, post));
    }

}
