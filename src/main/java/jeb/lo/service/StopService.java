package jeb.lo.service;

import jeb.lo.payload.response.StopResponse;
import jeb.lo.repository.StopRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StopService {

    private final StopRepository stopRepository;
    private final ModelMapper modelMapper;

    public StopService(StopRepository stopRepository, ModelMapper modelMapper) {
        this.stopRepository = stopRepository;
        this.modelMapper = modelMapper;
    }

    public List<StopResponse> getAllStops() {
        var allStops = stopRepository.findAll();
        return allStops.stream().map(stop -> modelMapper.map(stop, StopResponse.class)).collect(Collectors.toList());
    }
}
