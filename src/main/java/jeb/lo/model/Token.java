package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "tokens")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    TokenType type;

    String token;

    @ManyToOne
    Account account;
}
