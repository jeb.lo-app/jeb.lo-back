package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "lines")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Line {
    @Id
    String id;

    String end1;

    String end2;

    @OneToMany(mappedBy = "line")
    List<LineAffected> lineAffectedList;

}
