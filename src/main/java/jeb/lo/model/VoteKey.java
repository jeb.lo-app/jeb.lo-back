package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VoteKey implements Serializable {
    @Column(name = "account_id")
    Long accountId;

    @Column(name = "post_id")
    Long postId;
}
