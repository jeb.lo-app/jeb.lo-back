package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "reports")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Report {
    @EmbeddedId
    ReportKey id = new ReportKey();

    @ManyToOne
    @MapsId("accountId")
    @JoinColumn(name = "account_id")
    Account account;

    @ManyToOne
    @MapsId("postId")
    @JoinColumn(name = "post_id")
    Post post;

    EReportReason reason;

    String content;
}

