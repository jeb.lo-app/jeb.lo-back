package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stops")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Stop {
    @Id
    String Id;
    String stopCode;
    String stopName;
    Float stopLat;
    Float stopLon;
}
