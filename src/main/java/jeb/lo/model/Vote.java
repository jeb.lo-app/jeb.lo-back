package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "votes")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Vote {

    @EmbeddedId
    VoteKey id = new VoteKey();

    @ManyToOne
    @MapsId("accountId")
    @JoinColumn(name = "account_id")
    Account account;

    @ManyToOne
    @MapsId("postId")
    @JoinColumn(name = "post_id")
    Post post;

    Boolean isLike;
}

