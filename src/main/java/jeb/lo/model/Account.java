package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "accounts")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String photo = "https://jeblo-front-stage.herokuapp.com/assets/no-photo.png";

    @OneToOne(mappedBy = "account")
    @EqualsAndHashCode.Exclude
    User user;

    @OneToMany(mappedBy = "account")
    List<Post> posts;
    @OneToMany(mappedBy = "account")
    List<Vote> votes;
    @OneToMany(mappedBy = "account")
    List<Comment> comments;
    @OneToMany(mappedBy = "account")
    List<Token> tokens;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "line_favorite",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "line_id"))
    List<Line> lineFavoriteList;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "stop_favorite",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "stop_id"))
    List<Stop> stopFavoriteList;

}

