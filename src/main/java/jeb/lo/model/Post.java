package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "posts")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String title;
    String description;
    LocalDateTime postedDate = LocalDateTime.now();
    @Enumerated(EnumType.STRING)
    EStatusPost statusPost = EStatusPost.NOWE;
    Float latitude;
    Float longitude;

    @ManyToOne
    Account account;
    @ManyToOne
    Stop stop;
    @OneToMany(mappedBy = "post")
    @Cascade(CascadeType.DELETE)
    List<LineAffected> lineAffectedList;
    @OneToMany(mappedBy = "post")
    @Cascade(CascadeType.DELETE)
    List<Vote> votes;
    @Cascade(CascadeType.DELETE)
    @OneToMany(mappedBy = "post")
    List<PostPhoto> photos;
    @OneToMany(mappedBy = "post")
    @Cascade(CascadeType.DELETE)
    List<Comment> comments;
}
