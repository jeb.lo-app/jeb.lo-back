package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "lines_affected")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LineAffected {
    @EmbeddedId
    LineAffectedKey id = new LineAffectedKey();

    @ManyToOne
    @MapsId("lineId")
    @JoinColumn(name = "line_id")
    Line line;

    @ManyToOne
    @MapsId("postId")
    @JoinColumn(name = "post_id")
    Post post;

    LocalDateTime incidentDate;

}

