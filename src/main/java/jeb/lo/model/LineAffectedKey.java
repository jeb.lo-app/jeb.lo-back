package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LineAffectedKey implements Serializable {
    @Column(name = "line_id")
    String lineId;

    @Column(name = "post_id")
    Long postId;
}
