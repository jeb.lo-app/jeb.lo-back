package jeb.lo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "post_photos")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PostPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String url;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    Post post;
}
