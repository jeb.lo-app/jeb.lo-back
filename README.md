# je.blo Plecki

## Requirements:
- Docker (https://docs.docker.com/install/)
(Check if you have virtualization enabled in bios)

## Prepare
Copy `.env.example` to `.env`. 

## Running
`docker-compose up --build` to force rebuild.

`docker-compose up` in main folder or InteliJ configuration (I'll show you how to configure it)

`docker-compose up -d` if you don't need log on screen

## Commands inside container

`docker-compose exec <containerName> <command>` or `docker-compose exec -it <containerName> bash`

## Stoping
`docker-compose stop` to stop the containers

`docker-compose down` to stop and remove containers and networks

`docker-compose down --rmi all -v` to stop and remove everything
## Issues during docker initialize
In this order:
1. `docker-compose down`
2. `docker-compose down -v`
3. `docker-compose ps` & `docker rm <containerId>`
4. Contact me ;)

## Basic rules
- do NOT work or push directly to master or production
- merging with master will deploy to stage env
- merging with production will deploy to production env
- use gitLab issue->mr->deploy flow
- use semantic commit messages (https://seesparkbox.com/foundry/semantic_commit_messages)
- have fun ;)

## Other
Discord link: https://discord.gg/XdYUJm
